package com.test.dogbreeds

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

import kotlinx.android.synthetic.main.activity_sub_breed.*

class SubBreedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_breed)

        // Retrieve the serialized BreedModel
        val breed = intent.getSerializableExtra("breed") as BreedModel
        // Use the BreedModel to populate the list with its subbreeds
        populateList(breed)
    }

    private fun populateList(breed: BreedModel) {
        // Get the subbreed names and add them to a List<String>
        val subBreeds = (0..(breed.subBreeds.size - 1)).map {
            breed.getSubBreedName(it)
        }
        var breeds = subBreeds
        // Add the main breed if only one subbreed exists
        if (breed.subBreeds.size == 1) {
            breeds = listOf(breed.getName()) + breeds
        }
        // Initialize the adapter and set the ListView to use it
        runOnUiThread({
            val adapter = SubBreedAdapter(this, breeds)
            subBreedsList.adapter = adapter
        })
    }

    private class SubBreedAdapter(context: Context, subBreeds: List<String>): BaseAdapter() {
        private val context: Context = context
        private val subBreeds: List<String> = subBreeds

        override fun getCount(): Int {
            return subBreeds.size
        }

        override fun getItem(p0: Int): Any {
            return subBreeds[p0]
        }

        override fun getItemId(p0: Int): Long {
            return p0.toLong()
        }

        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
            val textView = TextView(context)
            textView.text = subBreeds[p0]
            textView.setOnClickListener({
                Toast.makeText(context, textView.text, Toast.LENGTH_SHORT).show()
            })
            textView.height = 200

            return textView
        }
    }
}
