package com.test.dogbreeds

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat.startActivity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.github.salomonbrys.kotson.*
import com.google.gson.*
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()

        // Calls the API and populates the list once an answer is received
        callApi()
    }

    fun populateList(jsonObject: JsonObject) {
        // Create a list of BreedModels
        var breeds: ArrayList<BreedModel> = arrayListOf()
        val gson = Gson()
        // Iterate over all the breeds in the JSON response and add them to the list above,
        // make sure to convert from JSON to List<String> to be able to serialize the data
        jsonObject.entrySet().map {
            breeds.add(BreedModel(it.key, gson.fromJson(jsonObject[it.key])))
        }
        // Initialize the adapter and set the ListView to use it
        runOnUiThread({
            val adapter = BreedAdapter(this, breeds)
            breedsList.adapter = adapter
        })
    }

    private fun callApi() {
        val client = OkHttpClient()
        val request = Request.Builder().url("https://dog.ceo/api/breeds/list/all").build()
        // Call the API and populate the list once it has been received
        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call?, e: IOException?) {}
            override fun onResponse(call: Call, response: Response) {
                // Convert the response from string to a JsonObject via JsonElement
                val element: JsonElement = JsonParser().parse(response.body()?.string())
                val jsonObject = element.asJsonObject
                // Make sure the call was successful
                val status = jsonObject["status"].asString
                if (status != "success") {
                    throw Exception("The call to the API was unsuccessful")
                }

                populateList(jsonObject["message"].asJsonObject)
            }
        })
    }

    private class BreedAdapter(context: Context, breeds: List<BreedModel>): BaseAdapter() {
        private val context: Context = context
        private val breeds: List<BreedModel> = breeds

        override fun getCount(): Int {
            return breeds.size
        }

        override fun getItem(p0: Int): Any {
            return breeds[p0]
        }

        override fun getItemId(p0: Int): Long {
            return p0.toLong()
        }

        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
            val textView = TextView(context)
            // Set the text to be the name of the breed
            textView.text = breeds[p0].getName()
            textView.setOnClickListener({
                // If the breed has a subbreed, start a new activity where the breed is passed on
                // as a serialized extra
                if (breeds[p0].subBreeds.isNotEmpty()) {
                    val intent = Intent(context, SubBreedActivity::class.java)
                    intent.putExtra("breed", breeds[p0])
                    startActivity(context, intent, null)
                // If the breed does not have a subbreed, display the text as a Toast notification
                } else {
                    Toast.makeText(context, textView.text, Toast.LENGTH_SHORT).show()
                }
            })
            // Change the height to make it easier to click the TextView
            textView.height = 200

            return textView
        }
    }
}
