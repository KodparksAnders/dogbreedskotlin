package com.test.dogbreeds

import java.io.Serializable

class BreedModel: Serializable {
    var key: String
    var subBreeds: List<String>

    constructor(key: String, subBreeds: List<String>) {
        this.key = key
        this.subBreeds = subBreeds
    }

    /*
     * Used by the getName/getSubBreedName functions to display the name in a more aesthetic way
     */
    private fun normalizeName(name: String): String {
        return name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase()
    }

    /*
     * Returns the breed name with the first letter in upper case
     */
    fun getName(): String {
        return normalizeName(key)
    }

    /*
     * Returns the subbreed name followed by the breed name with the first letter in upper case
     */
    fun getSubBreedName(position: Int): String {
        return normalizeName("${subBreeds[position]} ${key}")
    }
}